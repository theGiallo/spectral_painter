#pragma once

#include <JuceHeader.h>

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent  : public  juce::AudioAppComponent,
                       private juce::Timer
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent() override;

    //==============================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void getNextAudioBlock (const juce::AudioSourceChannelInfo& bufferToFill) override;
    void releaseResources() override;

    //==============================================================================
    void paint (juce::Graphics& g) override;
    void resized() override;

    void update();
    void generate_image();

    void timerCallback() override
    {
        update();
        repaint();
    }

	virtual void mouseMove(        const juce::MouseEvent & e );
	virtual void mouseEnter(       const juce::MouseEvent & e );
	virtual void mouseExit(        const juce::MouseEvent & e );
	virtual void mouseDown(        const juce::MouseEvent & e );
	virtual void mouseDrag(        const juce::MouseEvent & e );
	virtual void mouseUp(          const juce::MouseEvent & e );
	virtual void mouseDoubleClick( const juce::MouseEvent & e );
	virtual void mouseWheelMove(   const juce::MouseEvent & e, const juce::MouseWheelDetails & wheel );
	virtual void mouseMagnify(     const juce::MouseEvent & e, float scale_factor );

private:
    //==============================================================================
    // Your private member variables go here...

	static constexpr auto fft_order = 9;
	static constexpr auto fft_size  = 1 << fft_order;
	static constexpr auto spectrum_height = fft_size / 2 + 1;
	static constexpr auto samples_count = fft_size * 3;
	juce::Image             spectrum_render_image;
	juce::Image::BitmapData spectrum_render_data;
	juce::Image             computed_swave_image;
	juce::Image::BitmapData computed_swave_image_data;
	juce::Image             recomputed_spectrum_image;
	juce::Image::BitmapData recomputed_spectrum_image_data;
	juce::Image             recomputed_swave_image;
	juce::Image::BitmapData recomputed_swave_image_data;
	juce::MouseListener     mouse_listener;
	juce::int64             high_res_ticks;
	juce::int64             ticks_per_frame;
	const juce::int32       target_framerate = 60;
	juce::Label             frame_info_label;
	int                     tick_tock = 0;
	juce::dsp::FFT          forward_fft;
	juce::dsp::FFT          inverse_fft;
	float *                 window_values = 0;
	float *                 spectrogram_matrix = 0;
	float *                 recomputed_spectrogram_matrix = 0;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
