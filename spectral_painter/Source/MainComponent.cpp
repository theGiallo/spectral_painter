#include "MainComponent.h"
#include <cassert>
#include <cfloat>

static inline juce::Colour get_px( juce::Image::BitmapData & bmp, int x, int y )
{
	juce::Colour ret = bmp.getPixelColour( x, y );
	return ret;
}
static inline void set_px( juce::Image::BitmapData & bmp, int x, int y, juce::Colour c )
{
	bmp.setPixelColour( x, y, c );
}
static inline juce::Colour grayscale( juce::Colour c )
{
	int sum = int(c.getRed()) + int(c.getGreen()) + int(c.getBlue());
	juce::uint8 b = sum / 3;
	juce::Colour ret = juce::Colour( b, b, b, c.getAlpha() );
	return ret;
}
static inline int s32_min( int a, int b )
{
	return a < b ? a : b;
}
static inline int s32_max( int a, int b )
{
	return a > b ? a : b;
}
static inline float fmin( float a, float b )
{
	return a < b ? a : b;
}
static inline float fmax( float a, float b )
{
	return a > b ? a : b;
}
static inline float fclamp( float x, float a, float b )
{
	x = ( x > a ? x : a );
	x = ( x < b ? x : b );
	return x;
}
static inline int s32_sign( int x )
{
	return x > 0 ? 1 : ( x < 0 ? -1 : 0 );
}
static inline int signf( float x )
{
	return x > 0 ? 1 : ( x < 0 ? -1 : 0 );
}
static inline juce::Colour gray( float v )
{
	juce::uint8 b = juce::uint8( int( fclamp( v * 256, 0, 255 ) ) );
	juce::Colour ret = juce::Colour( b, b, b, juce::uint8(0xff) );
	return ret;
}
static inline float f_gray( juce::Colour c )
{
	float ret = c.getRed() / float(255);
	return ret;
}
static inline void column_from_bmp( juce::Image::BitmapData & bmp, int column_index, float * column )
{
	int height    = bmp.height;
	//int width     = bmp.width;
	assert( column_index >= 0 );
	assert( column_index < bmp.width );
	for ( int y = 0; y != height; ++y )
	{
		column[y] = f_gray( get_px( bmp, column_index, y ) );
	}
}
static inline void column_into_bmp( juce::Image::BitmapData & bmp, int column_index, float * column )
{
	int height    = bmp.height;
	//int width     = bmp.width;
	assert( column_index >= 0 );
	assert( column_index < bmp.width );
	for ( int y = 0; y != height; ++y )
	{
		set_px( bmp, column_index, y, gray( column[y] ) );
	}
}
static inline void column_into_bmp( juce::Image::BitmapData & bmp, int column_index, float * column, int stride )
{
	int height    = bmp.height;
	//int width     = bmp.width;
	assert( column_index >= 0 );
	assert( column_index < bmp.width );
	for ( int y = 0; y != height; ++y )
	{
		set_px( bmp, column_index, y, gray( column[y * stride] ) );
	}
}
static inline void column_into_matrix( float * matrix, int width, int height, int column_index, float * column )
{
	assert( column_index >= 0 );
	assert( column_index < width );
	for ( int y = 0; y != height; ++y )
	{
		matrix[ y * width + column_index ] = column[y];
	}
}
static inline void column_from_matrix( float * matrix, int width, int height, int column_index, float * column )
{
	for ( int y = 0; y != height; ++y )
	{
		column[y] = matrix[ y * width + column_index ];
	}
}
static inline void bmp_from_fmatrix_normalize( juce::Image::BitmapData & bmp, float * matrix )
{
	int height = bmp.height;
	int width  = bmp.width;

	float min = FLT_MAX;
	float max = -FLT_MAX;

	for ( int y = 0; y != height; ++y )
	for ( int x = 0; x != width; ++x )
	{
		float v = matrix[ y * width + x ];
		min = fmin( min, v );
		max = fmax( max, v );
	}

	float span = max - min;

	for ( int y = 0; y != height; ++y )
	for ( int x = 0; x != width; ++x )
	{
		float v = matrix[ y * width + x ];
		v = ( v - min ) / span;
		set_px( bmp, x, y, gray( v ) );
	}
}
static inline void bmp_from_fmatrix( juce::Image::BitmapData & bmp, float * matrix )
{
	int height = bmp.height;
	int width  = bmp.width;
	for ( int y = 0; y != height; ++y )
	for ( int x = 0; x != width; ++x )
	{
		set_px( bmp, x, y, gray( matrix[ y * width + x ] ) );
	}
}
static inline void draw_soundwave( juce::Image::BitmapData & bmp, float * samples, int samples_count )
{
	int height = bmp.height;
	int width  = bmp.width;
	int h_height = height / 2;

	for ( int y = 0; y != height; ++y )
	for ( int x = 0; x != width; ++x )
	{
		float sample = samples[x];

		float s = fclamp( sample, -1, 1 );
		int px_h = y - h_height;
		float px = ( y - h_height ) / float(h_height);
		float sign_px  = signf( px );
		float sign_s   = signf( s  );
		bool in_area =
		   ( s >= 0 && px >= 0 && px <= s )
		|| ( s <= 0 && px <= 0 && px >= s );
		juce::Colour c = in_area ? gray( 1 ) : juce::Colours::black;
		set_px( bmp, x, y, c );
	}
}

static inline void normalize_grays( juce::Image::BitmapData & bmp )
{
	float min_v = FLT_MAX;
	float max_v = -FLT_MAX;
	for ( int y = 0; y != bmp.height; ++y )
	for ( int x = 0; x != bmp.width; ++x )
	{
		juce::Colour c = get_px( bmp, x, y );
		float r = c.getRed() / 255.f;
		float gray_value = r;
		min_v = fmin( min_v, gray_value );
		max_v = fmax( max_v, gray_value );
	}

	//printf( "min gray: %f max gray: %f\n", min_v, max_v );

	float gray_span = max_v - min_v;

	for ( int y = 0; y != bmp.height; ++y )
	for ( int x = 0; x != bmp.width; ++x )
	{
		juce::Colour c = get_px( bmp, x, y );
		juce::uint8 a = c.getAlpha();
		float r = c.getRed() / 255.f;
		float gray_value = r;
		gray_value = ( gray_value - min_v ) / gray_span;
		c = gray( gray_value );
		c = c.withAlpha( a );
	}
}
static inline void print_min_max( float * arr, int count, const char * name = "" )
{
	float min_v = FLT_MAX;
	float max_v = -FLT_MAX;
	for ( int i = 0; i != count; ++i )
	{
		float v = arr[i];
		min_v = fmin( min_v, v );
		max_v = fmax( max_v, v );
	}

	printf( "%s min: %f max: %f\n", name, min_v, max_v );
}
static inline float window_at( float a, int index, int count )
{
	const float n = index;
	const float N = count - 1;
	const float tau = 6.2831853071f;
	float ret = a + ( 1 - a ) * cos( tau * n / N );
	return ret;
}
static inline void window_fill( float * w, int count, float a )
{
	for ( int n = 0; n != count; ++n )
	{
		w[n] = window_at( a, n, count );
	}
}
static inline void window_fill_hann( float * w, int count )
{
	window_fill( w, count, 0.5f );
}
static inline void window_fill_hamming( float * w, int count )
{
	window_fill( w, count, 25.f / 46.f );
}


// NOTE(theGiallo): Although performRealOnlyInverseTransform will only use the first ((size / 2) + 1) complex numbers,
// the size of the array passed in must still be 2 * getSize(), as some FFT engines require the extra space for the calculation.
// On return, the first half of the array will contain the reconstituted samples.
// float * spectrogram_column = (float*)calloc( spectrogram_height * 2, sizeof(float) );
// float * samples = (float*)calloc( spectrogram_width, sizeof(float) );
//
// TODO(theGiallo, 2020-12-19): if we used a window in the stft, we have to use it too
// TODO(theGiallo, 2020-12-19): we seem to be having the frequency doubled while doing stft->swave->stft->swave
static inline void inverse_stft( juce::dsp::FFT & inverse_fft,
                                 float * spectrogram_matrix,
                                 int     spectrogram_width,
                                 int     spectrogram_height,
                                 float * spectrogram_column,
                                 float * out_samples )
{
	int fft_size = spectrogram_height;
	assert( fft_size == inverse_fft.getSize() );

	for ( int column_index = 0; column_index != spectrogram_width; ++column_index )
	{
		//column_from_bmp( spectrum_render_data, column_index, spectrogram_column );
		column_from_matrix( spectrogram_matrix, spectrogram_width, spectrogram_height, column_index, spectrogram_column );

		// NOTE(theGiallo): this is for when the range is 0-1
		#if 0
		for ( int i = 0; i != spectrogram_height; ++i )
		{
			const float tau = 6.2831853071f;
			const float pi  = 3.14159265f;
			spectrogram_column[i] *= spectrogram_height;
		}
		#endif

		inverse_fft.performRealOnlyInverseTransform( spectrogram_column );
		float * reconstructed_samples_window = spectrogram_column;

		// TODO(theGiallo, 2020-12-19): this is clearly wrong
		float sample = reconstructed_samples_window[column_index % (fft_size)];
		//sample *= 1e1f;
		out_samples[column_index] = sample;

		#if 0
		min_sample = fmin( sample, min_sample );
		max_sample = fmax( sample, max_sample );

		bool draw_ifft_vertical = false;
		bool draw_soundwave     = true;
		if ( draw_ifft_vertical )
		{
			for ( int i = 0; i != computed_swave_image_data.height; ++i )
			{
				set_px( computed_swave_image_data, column_index, i, gray( reconstructed_samples_window[i] ) );
			}
		} else
		if ( draw_soundwave )
		{
			int height = computed_swave_image_data.height;
			int h_height = height / 2;
			float s = fclamp( sample, -1, 1 );
			for ( int i = 0; i != height; ++i )
			{
				int px_h = i - h_height;
				float px = ( i - h_height ) / float(h_height);
				float sign_px  = signf( px );
				float sign_s   = signf( s  );
				bool in_area =
				   ( s >= 0 && px >= 0 && px <= s )
				|| ( s <= 0 && px <= 0 && px >= s );
				juce::Colour c = in_area ? gray( 1 ) : juce::Colours::black;
				set_px( computed_swave_image_data, column_index, i, c );
			}
		}
		#endif
	}
}

enum WindowType
{
	NONE,
	HANN,
	HAMMING,
};
// NOTE(theGiallo): spectrogram_matrix has width = samples_count and height = forward_fft.getSize() / 2 + 1
//
// NOTE(theGiallo): The size of the array passed in must be 2 * getSize(),
// and the first half should contain your raw input sample data.
//
// int const samples_window_count = forward_fft.getSize() * 2;
// float * samples_window = (float*) calloc( samples_window_count, sizeof(float) );
//
// NOTE(theGiallo): float * cut_window = (float*) calloc( M, sizeof ( float ) );
template<WindowType wt>
static inline void stft( juce::dsp::FFT & forward_fft,
                         float * samples,
                         int     samples_count,
                         float * spectrogram_matrix,
                         float * samples_window,
                         float * cut_window,
                         int     M
                       )
{
	int const fft_size = forward_fft.getSize();
	int const samples_window_count = fft_size * 2;
	int const Mh = M / 2;
	int const spectrogram_width  = samples_count;
	int const spectrogram_height = fft_size / 2 + 1;

	int prev_samples_in_window_count = 0;
	for ( int sample_index = 0; sample_index != samples_count; ++sample_index )
	{
		memset( samples_window, 0x0, samples_window_count * sizeof( samples_window[0] ) );

		int samples_w_start = sample_index - Mh;
		int samples_w_end   = sample_index + Mh + 1;
		int samples_start   = s32_max( 0,             samples_w_start );
		int samples_end     = s32_min( samples_count, samples_w_end );
		int cut_samples_start = samples_start - samples_w_start;
		int cut_samples_end   = samples_w_end - samples_end;
		int N = fft_size;
		int padding_half_length = ( N - M ) / 2;
		int wi_start = padding_half_length + cut_samples_start;
		int samples_in_window_count = samples_end - samples_start;

		if ( samples_in_window_count != prev_samples_in_window_count )
		{
			if constexpr ( wt == WindowType::HANN )
			{
				window_fill_hann( cut_window, samples_in_window_count );
			} else
			if constexpr ( wt == WindowType::HAMMING )
			{
				window_fill_hamming( cut_window, samples_in_window_count );
			}
		}
		prev_samples_in_window_count = samples_in_window_count;

		//printf( "[%2d] wi_start: %d samples_start: %d samples_w_start: %d samples_end: %d, samples_w_end: %d\n",
		//        sample_index, wi_start, samples_start, samples_w_start, samples_end, samples_w_end );

		for ( int si = samples_start, wi = wi_start, ci = 0;
		      si != samples_end;
		      ++si, ++wi, ++ci )
		{
			//samples_window[wi] = samples[si];// * window_values[wi - padding_half_length];
			if constexpr ( wt == WindowType::NONE )
			{
				samples_window[wi] = samples[si];
			} else
			{
				samples_window[wi] = samples[si] * cut_window[ci];
			}
		}

		//print_min_max( samples + samples_start, samples_end - samples_start, "samples[window]" );
		//print_min_max( samples_window, samples_window_count, "samples_window" );

		#if 1
		forward_fft.performRealOnlyForwardTransform( samples_window, true /*dontCalculateNegativeFrequencies*/ );
		#else
		// NOTE(theGiallo): not this. It seems it's 0.5 scaled on y.
		forward_fft.performFrequencyOnlyForwardTransform( samples_window );
		#endif

		//print_min_max( samples_window, samples_window_count, "spectrogram_column" );

		float * spectrogram_column = samples_window;
		//column_into_bmp( recomputed_spectrum_image_data, sample_index, spectrogram_column );
		column_into_matrix( spectrogram_matrix, spectrogram_width, spectrogram_height, sample_index, spectrogram_column );
	}

	//normalize_grays( recomputed_spectrum_image_data );
}


//==============================================================================
MainComponent::MainComponent() :
                                 spectrum_render_image         ( juce::Image::RGB, samples_count, spectrum_height, true, juce::SoftwareImageType() ),
                                 spectrum_render_data          ( spectrum_render_image,     juce::Image::BitmapData::ReadWriteMode::readWrite ),
                                 computed_swave_image          ( juce::Image::RGB, samples_count, fft_size,        true, juce::SoftwareImageType() ),
                                 computed_swave_image_data     ( computed_swave_image,      juce::Image::BitmapData::ReadWriteMode::readWrite ),
                                 recomputed_spectrum_image     ( juce::Image::RGB, samples_count, spectrum_height, true, juce::SoftwareImageType() ),
                                 recomputed_spectrum_image_data( recomputed_spectrum_image, juce::Image::BitmapData::ReadWriteMode::readWrite ),
                                 recomputed_swave_image        ( juce::Image::RGB, samples_count, fft_size,        true, juce::SoftwareImageType() ),
                                 recomputed_swave_image_data   ( recomputed_swave_image,    juce::Image::BitmapData::ReadWriteMode::readWrite ),
                                 high_res_ticks(juce::Time::getHighResolutionTicks()),
                                 frame_info_label(),
                                 forward_fft( MainComponent::fft_order ),
                                 inverse_fft( MainComponent::fft_order ),
                                 spectrogram_matrix(           (float*)calloc( spectrum_height * samples_count, sizeof(*spectrogram_matrix))),
                                 recomputed_spectrogram_matrix((float*)calloc( spectrum_height * samples_count, sizeof(*spectrogram_matrix)))
{
    // Make sure you set the size of the component after
    // you add any child components.
    //setSize (800, 600);

	for ( int y = 0; y != spectrum_render_data.height; ++y )
	for ( int x = 0; x != spectrum_render_data.width; ++x )
	{
		set_px( spectrum_render_data, x, y, juce::Colours::black );
	}

    setOpaque( true );
    setSize( spectrum_render_image.getWidth() * 2, spectrum_render_image.getHeight() * 3 );
    setAudioChannels( 2, 0 );

    startTimerHz( target_framerate );

    // Some platforms require permissions to open input channels so request that here
    if (juce::RuntimePermissions::isRequired (juce::RuntimePermissions::recordAudio)
        && ! juce::RuntimePermissions::isGranted (juce::RuntimePermissions::recordAudio))
    {
        juce::RuntimePermissions::request (juce::RuntimePermissions::recordAudio,
                                           [&] (bool granted) { setAudioChannels (granted ? 2 : 0, 2); });
    }
    else
    {
        // Specify the number of input and output channels that we want to open
        setAudioChannels (2, 2);
    }

	juce::int64 ticks_per_sec = juce::Time::getHighResolutionTicksPerSecond();
	ticks_per_frame = ticks_per_sec / target_framerate;

	addAndMakeVisible( frame_info_label );

	printf( "forward fft getSize(): %d\n", forward_fft.getSize() );
	printf( "inverse fft getSize(): %d\n", inverse_fft.getSize() );
}

MainComponent::~MainComponent()
{
    // This shuts down the audio device and clears the audio source.
    shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    // This function will be called when the audio device is started, or when
    // its settings (i.e. sample rate, block size, etc) are changed.

    // You can use this function to initialise any resources you might need,
    // but be careful - it will be called on the audio thread, not the GUI thread.

    // For more details, see the help for AudioProcessor::prepareToPlay()
    (void)samplesPerBlockExpected;
    (void)sampleRate;
}

void MainComponent::getNextAudioBlock (const juce::AudioSourceChannelInfo& bufferToFill)
{
    // Your audio-processing code goes here!

    // For more details, see the help for AudioProcessor::getNextAudioBlock()

    // Right now we are not producing any data, in which case we need to clear the buffer
    // (to prevent the output of random noise)
    bufferToFill.clearActiveBufferRegion();
}

void MainComponent::releaseResources()
{
    // This will be called when the audio device stops, or when it is being
    // restarted due to a setting change.

    // For more details, see the help for AudioProcessor::releaseResources()
}

//==============================================================================
void MainComponent::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    // You can add your drawing code here!
    g.setOpacity( 1.0f );

	//g.drawImage( spectrum_render_image, getLocalBounds().toFloat() );
	//{
	//	int w = spectrum_render_image.getWidth();
	//	int h = spectrum_render_image.getHeight();
	//	g.drawImage( spectrum_render_image, w, 0, w, h, 0, 0, w, h );
	//}
	g.drawImageAt( spectrum_render_image, 0, 0 );
    g.drawImageAt( computed_swave_image,   0, spectrum_render_data.height );
    g.drawImageAt( recomputed_spectrum_image, computed_swave_image_data.width, 0 );
    g.drawImageAt( recomputed_swave_image, computed_swave_image_data.width, spectrum_render_data.height );
}

void MainComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.

	printf( "resized\n" );

	int lw, lh, margin_top;
	lw = 400;
	int lines_count = 4;
	lh = frame_info_label.getFont().getHeight() * lines_count;
	margin_top = 10;
	frame_info_label.setBounds( getWidth() - lw, margin_top, lw, lh );
}

static int sample_index = 0;
static int sample_index_tick_tock = 0;
void MainComponent::update()
{
	juce::int64 start = juce::Time::getHighResolutionTicks();

	generate_image();

	float min_sample = FLT_MAX;
	float max_sample = -FLT_MAX;

	// TODO(theGiallo, 2020-12-10): experimenting with STFT implementation, not working
	{
		int sampling_freq = fft_size;
		if ( sampling_freq <= 0 )
		{
			fprintf( stderr, "sampling_freq = %d\n", sampling_freq );
			return;
		}

		// TODO(theGiallo, 2020-12-14): use W = N + 1
		//int window_length = ( sampling_freq / 8 ) | 1; // NOTE(theGiallo): window has to be odd
		int window_length = sampling_freq - 1;

		int N = sampling_freq;
		int Nh = N / 2;
		int M = window_length;
		int Mh = M / 2;
		int R = 1;
		//int padding_half_length = ( N - M ) / 2;

		if ( ! window_values )
		{
			window_values = (float*)calloc( M, sizeof(float) );
			window_fill_hamming( window_values, M );
			//window_fill_hann   ( window_values, M );
		}

		#if 0
		printf( "samples count: %d\n", samples_count );
		printf( "N:  %d\n", N );
		printf( "Nh: %d\n", Nh );
		printf( "M:  %d\n", M );
		printf( "Mh: %d\n", Mh );
		printf( "R:  %d\n", R );
		printf( "padding half: %d\n", padding_half_length );
		#endif

		const int columns_count = samples_count;
		static float * samples = (float*)calloc( columns_count, sizeof(float) );
		memset( samples, 0x0, columns_count * sizeof( samples[0] ) );

		static float * spectrogram_column_hook = (float*)calloc( sampling_freq * 2, sizeof(float) );
		memset( spectrogram_column_hook, 0x0, sampling_freq * 2 * sizeof( spectrogram_column_hook[0] ) );
		float * spectrogram_column = spectrogram_column_hook;

		int spectrogram_width  = samples_count;
		int spectrogram_height = spectrum_height;

		inverse_stft( inverse_fft,
		              spectrogram_matrix, // float *
		              spectrogram_width,  // int
		              spectrogram_height, // int
		              spectrogram_column, // float *
		              samples );          // float *

		draw_soundwave( computed_swave_image_data, samples, samples_count );

		//print_min_max( samples, samples_count, "           samples" );

		int const samples_window_count = sampling_freq * 2;
		static float * samples_window = (float*) calloc( samples_window_count, sizeof(float) );
		memset( samples_window, 0x0, samples_window_count * sizeof( samples_window[0] ) );

		static float * cut_window = (float*) calloc( M, sizeof ( float ) );


		stft<WindowType::NONE>( forward_fft,
		                        samples,            // float *
		                        samples_count,      // int
		                        recomputed_spectrogram_matrix, // float *
		                        samples_window,     // float *
		                        cut_window,         // float *
		                        M );                // int

		bmp_from_fmatrix_normalize( recomputed_spectrum_image_data, recomputed_spectrogram_matrix );

		memset( samples, 0x0, sizeof( float ) * samples_count );
		inverse_stft( inverse_fft,
		              recomputed_spectrogram_matrix, // float *
		              spectrogram_width,  // int
		              spectrogram_height, // int
		              spectrogram_column, // float *
		              samples );          // float *

		draw_soundwave( recomputed_swave_image_data, samples, samples_count );


		//print_min_max( samples, samples_count, "recomputed samples" );

		#if 0
		free( samples );
		free( spectrogram_column_hook );
		#endif
	}

	juce::int64 end = juce::Time::getHighResolutionTicks();

	float ms = juce::Time::highResolutionTicksToSeconds( end - start ) * 1000.0f;
	float frame_ms = juce::Time::highResolutionTicksToSeconds( end - high_res_ticks ) * 1000.0f;
	high_res_ticks = end;

	juce::String label_text = "";
	label_text += juce::String( ms, 2 );
	label_text += " ms | ";
	label_text += juce::String( frame_ms, 2 );
	label_text += " ms\n";
	label_text += "min ";
	label_text += juce::String( min_sample );
	label_text += "\nmax ";
	label_text += juce::String( max_sample );
	label_text += "\nsample idx ";
	label_text += juce::String( sample_index );
	frame_info_label.setText( label_text, juce::NotificationType::dontSendNotification );

	const int sample_index_tick_tock_max = 60 / 10;
	sample_index_tick_tock = sample_index_tick_tock == sample_index_tick_tock_max - 1 ? 0 : sample_index_tick_tock + 1;
	if ( sample_index_tick_tock == 0 )
	{
		sample_index = sample_index == spectrum_render_data.height - 1 ? 0 : sample_index + 1;
	}
}

#if 0
float f( int the_h )
{
    float ret = 0;
    int h = the_h;
    int w = the_h;
    for ( int i = 0; i < h; ++i )
    for ( int j = 0; j < w; ++j )
    {
        float h, s, v;
        float i01 = i / float(h);
        float j01 = j / float(w);
        h = i / 2.0f;
        s = i * i;
        v = 1;
        ret += h + s + v + i01 + j01;
    }
    return ret;
}
#endif

void MainComponent::generate_image()
{
	const int tt_s = 240;
	//++tick_tock; // TODO(theGiallo, 2020-12-12): disabled this to debug stft
	tick_tock = tick_tock == tt_s ? 0 : tick_tock;

	if ( tick_tock % ( tt_s / 6 ) != 0 )
	{
		return;
	}

	{
		juce::uint8 * ps = spectrum_render_data.data;
		int height    = spectrum_render_data.height;
		int width     = spectrum_render_data.width;
		int px_stride = spectrum_render_data.pixelStride;
		int line_pad = spectrum_render_data.lineStride - width * px_stride;
		int px_i = 0;
		int r_i = juce::PixelRGB::indexR;
		int g_i = juce::PixelRGB::indexG;
		int b_i = juce::PixelRGB::indexB;
		auto format = spectrum_render_data.pixelFormat;

		static bool first_time = true;
		if ( first_time )
		{
			first_time = false;
			printf( "w: %d; h: %d; line stride: %d; px_stride: %d; line pad: %d\n",
			        width, height,
			        spectrum_render_data.lineStride,
			        px_stride,
			        line_pad );
		}
		if ( format != juce::Image::PixelFormat::RGB )
		{
			fprintf( stderr, "wrong pixel format assumed" __FILE__ "@%d", __LINE__ );
		}

		for ( int y = 0; y != height; ++y, px_i += line_pad )
		{
			for ( int x = 0; x != width; ++x, px_i += px_stride )
			{
				float x01 = x / float(width);
				float y01 = y / float(height);

				#if 1
				float v = y == sample_index ? 1 : 0;
				spectrogram_matrix[ y * width + x ] = v * height;
				juce::Colour c = gray( v );
				#else
				juce::Colour c = gray( y == int ( x / float( sample_index / 4.f ) ) ? 1 : 0 );
				#endif

				ps[ px_i + r_i ] = c.getRed();
				ps[ px_i + g_i ] = c.getGreen();
				ps[ px_i + b_i ] = c.getBlue();

				//ps[ px_i + r_i ] = x01 * 255;
				//ps[ px_i + g_i ] = y01 * 255;
				//ps[ px_i + b_i ] = 0;
			}
		}

		return;
	}

	if ( tick_tock > tt_s / 2 )
	{
		// NOTE(theGiallo): this is ugly and only for testing :D
		int width  = spectrum_render_image.getWidth();
		int height = spectrum_render_image.getHeight();
		for ( int y = 0; y < height; ++y )
		{
			for ( int x = 0; x < width; ++x )
			{
				float x01 = x / float(width);
				float y01 = y / float(height);
				float h, s, v;
				h = x01;
				s = y01;
				v = 1;
				juce::uint8 r = x01 * 255;
				juce::uint8 g = x01 * 255;
				juce::uint8 b = x01 * 255;

				if ( tick_tock >  4 * tt_s / 6 )
				{
					r = y01 * 255;
					g = y01 * 255;
					b = y01 * 255;
				}

				juce::Colour c = juce::Colour::fromRGBA( r, g, b, 255 );

				bool use_hsv = tick_tock > 5 * tt_s / 6;
				if ( use_hsv )
				{
					c = juce::Colour::fromHSV( h, s, v, 1.0f);
				}

				c = grayscale( c );

				//spectrum_render_image.setPixelAt( x, y, c );
				set_px( spectrum_render_data, x, y, c );

				juce::Colour wc = get_px( spectrum_render_data, x, y );
				if ( ! use_hsv )
				{
					if ( c.getRed() != r )
					{
						fprintf( stderr, "red WHAT THE FUCK\n" );
					}
					if ( c.getGreen() != g )
					{
						fprintf( stderr, "green WHAT THE FUCK\n" );
					}
					if ( c.getBlue() != b )
					{
						fprintf( stderr, "blue WHAT THE FUCK\n" );
					}
				}
				if ( wc != c )
				{
					fprintf( stderr, "WHAT THE FUCK\n" );
				}
			}
		}
	} else
	{
		juce::uint8 * ps = spectrum_render_data.data;
		int height    = spectrum_render_data.height;
		int width     = spectrum_render_data.width;
		int px_stride = spectrum_render_data.pixelStride;
		int line_pad = spectrum_render_data.lineStride - width * px_stride;
		int px_i = 0;
		int r_i = juce::PixelRGB::indexR;
		int g_i = juce::PixelRGB::indexG;
		int b_i = juce::PixelRGB::indexB;
		auto format = spectrum_render_data.pixelFormat;

		if ( tick_tock == 0 )
		{
			printf( "w: %d; h: %d; line stride: %d; px_stride: %d; line pad: %d\n",
					width, height,
					spectrum_render_data.lineStride,
					px_stride,
					line_pad );
		}
		if ( format != juce::Image::PixelFormat::RGB )
		{
			fprintf( stderr, "wrong pixel format assumed" __FILE__ "@%d", __LINE__ );
		}

		for ( int y = 0; y != height; ++y, px_i += line_pad )
		{
			for ( int x = 0; x != width; ++x, px_i += px_stride )
			{
				float x01 = x / float(width);
				float y01 = y / float(height);
				float h, s, v;
				h = x01;
				s = y01;
				v = 1;

				auto c = juce::Colour::fromHSV( h, s, v, 1 );

				c = grayscale( c );

				ps[ px_i + r_i ] = c.getRed();
				ps[ px_i + g_i ] = c.getGreen();
				ps[ px_i + b_i ] = c.getBlue();

				//ps[ px_i + r_i ] = x01 * 255;
				//ps[ px_i + g_i ] = y01 * 255;
				//ps[ px_i + b_i ] = 0;
			}
		}
	}
}



void MainComponent::mouseMove( const juce::MouseEvent & e )
{
	(void)e;
}
void MainComponent::mouseEnter( const juce::MouseEvent & e )
{
	(void)e;
}
void MainComponent::mouseExit( const juce::MouseEvent & e )
{
	(void)e;
}
void MainComponent::mouseDown( const juce::MouseEvent & e )
{
	juce::Point<int> m = e.getPosition();
	//printf( "mouse down @ ( %d, %d )\n", m.x, m.y );

	if ( m.x < spectrum_render_data.width && m.y < spectrum_render_data.height )
	{
		set_px( spectrum_render_data, m.x, m.y, juce::Colours::red );
	}
}
void MainComponent::mouseDrag( const juce::MouseEvent & e )
{
	juce::Point<int> m = e.getPosition();
	//printf( "mouse drag @ ( %d, %d )\n", m.x, m.y );

	if ( m.x < spectrum_render_data.width && m.y < spectrum_render_data.height )
	{
		set_px( spectrum_render_data, m.x, m.y, gray( 0.5f ) );
	}
}
void MainComponent::mouseUp( const juce::MouseEvent & e )
{
	juce::Point<int> m = e.getPosition();
	//printf( "mouse up @ ( %d, %d )\n", m.x, m.y );

	if ( m.x < spectrum_render_data.width && m.y < spectrum_render_data.height )
	{
		set_px( spectrum_render_data, m.x, m.y, juce::Colours::green );
	}
}
void MainComponent::mouseDoubleClick( const juce::MouseEvent & e )
{
	(void)e;
}
void MainComponent::mouseWheelMove( const juce::MouseEvent & e, const juce::MouseWheelDetails & wheel )
{
	(void)e;
	(void)wheel;
}
void MainComponent::mouseMagnify( const juce::MouseEvent & e, float scale_factor )
{
	(void)e;
	(void)scale_factor;
}
